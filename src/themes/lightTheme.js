import { createTheme } from "@mui/material/styles";

const lightTheme = createTheme({
  palette: {
    primary: {
      main: "hsla(272, 14%, 49%, 1)",
    },
    secondary: {
      main: "hsla(44, 88%, 66%, 1)",
    },
    background: {
      default: "hsla(150, 14%, 84%, 1)",
    },
    text: {
      primary: "#hsla(60, 1%, 14%, 1)",
    },
  },
});

export default lightTheme;
