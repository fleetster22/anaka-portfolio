import { createTheme } from "@mui/material/styles";

const darkTheme = createTheme({
  palette: {
    primary: {
      main: "#007BFF",
    },
    secondary: {
      main: "hsla(272, 14%, 49%, 1)",
    },
    background: {
      default: "#hsla(60, 1%, 14%, 1)",
    },
    text: {
      primary: "#hsla(81, 28%, 90%, 1)",
    },
  },
});

export default darkTheme;
