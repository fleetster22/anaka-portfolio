import './App.css'
import AboutMe from './components/AboutMe/AboutMe.jsx'
import NavBar from './components/Header/NavBar.jsx'
import Footer from './components/Footer/Footer.jsx'
import { ThemeProvider, createTheme } from '@mui/material/styles';

const lightTheme = createTheme({
  palette: {
    primary: {
      main: red[500],
    },
  },
});

const darkTheme = createTheme({
  palette: {
    primary: {
      main: red[500],
    },
  },
});


function App() {
  return (
<ThemeProvider theme={lightTheme}>
    <div className='app'>
      <span><link rel="icon" type="image/x-icon" href="./assets/images/tacohackrfavicon.png"></link></span>
      <NavBar />
      <AboutMe />
      <Footer />
    </div>
    </ThemeProvider>
  )
}

export default App;
