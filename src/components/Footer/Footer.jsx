import React from 'react';

function Footer() {
  return (
    <footer className="footer">
      <div className="footer-content">
        <p>&copy; {new Date().getFullYear()} Anaka Norfleet. All rights reserved.</p>
        <div className="footer-social">
          <a href="https://www.linkedin.com/in/anaka-norfleet/" target="_blank" rel="noopener noreferrer">LinkedIn</a>
          <a href="https://github.com/fleetster22" target="_blank" rel="noopener noreferrer">GitHub</a>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
