import React from 'react'
import { Typography } from '@mui/material';

function AboutMe() {
    return (
      <div className="aboutMe">
      <Typography variant='h1' gutterBottom>About Me</Typography>
        <div className="aboutMe-content">
          <div className="aboutMe-image">
            <img src="./src/assets/images/AnakaHS.png" alt="Anaka Norfleet"/>
          </div>

          <div className="aboutMe-info">
            <p>
              Hello! I'm Anaka, a passionate application developer based in Denver, CO. I have a strong foundation in
              backend technologies, UI/UX design, and enjoy creating efficient, scalable, and maintainable web applications.
            </p>
            <p>
              Over the years, I've worked with a diverse set of clients and teams, bringing their web ideas to life.
              I specialize in Python, Django, ReactJS, FastAPI, Docker, and love exploring new technologies and tools in the web development space.
            </p>
          </div>
        </div>
      </div>
    );
  }

  export default AboutMe;
