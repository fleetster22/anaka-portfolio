import React from 'react';


function NavBar() {
  return (
    <div>
      <nav>
        <img src="./src/assets/images/tacohackrfavicon.png"  alt="tacohackr logo" class="IconButton"  /><span className="header-logo-text"></span>
        <ul>
          <li><a  href="#about">About Me</a></li>
          <li><a  href="#projects">Projects</a></li>
          <li><a  href="#contact">Contact</a></li>
          <li><a  href="#blog"></a></li>
        </ul>
      </nav>
      </div>
  );
}

export default NavBar;
